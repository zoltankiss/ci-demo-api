#!/bin/bash
set -e

nc -l 9248
cat $CI_PROJECT_DIR/hosts > /etc/hosts

export POSTGRES_HOST=`cat /etc/hosts |grep postgres|cut -d$'\t' -f1`

bundle exec rails db:create
bundle exec rails db:migrate
bundle exec rails db:seed

exec "$@"
