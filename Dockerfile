FROM ruby:2.5.1

# Update the system & add essential libs and programs
RUN apt-get update \
&& apt-get install -y --no-install-recommends netcat-openbsd \
  apt-transport-https \
  build-essential \
  libgmp-dev \
  postgresql-client \
  software-properties-common \
  vim \
&& apt-get clean autoclean -y \
&& apt-get autoremove -y

# Create directory where app will live
ENV APP_PATH /app
RUN mkdir -p $APP_PATH
WORKDIR $APP_PATH

RUN mkdir -p $APP_PATH/tmp && chmod 777 $APP_PATH/tmp \
  && mkdir -p $APP_PATH/log && chmod 777 $APP_PATH/log

ENV BUNDLE_PATH /app/ruby_gems
ENV WORKER_PROCESSES 1
ENV LISTEN_ON 0.0.0.0:3013

COPY Gemfile .
COPY Gemfile.lock .

ARG BUNDLE_DEPLOYMENT="--deployment"

# Install Gems
RUN gem install bundler --no-document \
  && bundle install

# Copy the actual rails app to the image
COPY . .

EXPOSE 3013

ENTRYPOINT ["/app/entrypoint.sh"]

CMD ["bin/rails", "s", "-p", "3013", "-b", "0.0.0.0"]
